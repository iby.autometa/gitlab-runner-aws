output "sg-id" {
  value =  aws_security_group.runner-sg.id
  description = "The ID of the runner security group"
}

#output "sg_id" {
#  description = "The ID of the security group"
#  value = concat(
#    aws_security_group.runner-sg.id
#  )
#}
